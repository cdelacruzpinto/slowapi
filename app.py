from flask import Flask, request, make_response
from flask_restful import Resource, Api
import string
import json
import random
import time
app = Flask(__name__)
api = Api(app)

users = dict()
user_id = 1

def random_string(length, type = None):
  # choose from all lowercase letter
  if type == None or type not in [string.ascii_letters, string.digits]:
    type = string.ascii_letters
  return ''.join(random.choice(type) for i in range(length))

class Users(Resource):
    def get(self):
      global users
      user_ids = [ dict({"id": key}) for key, user in users.items() ] 
      return make_response(json.dumps(user_ids), 200)
    def post(self):
      global user_id
      users[str(user_id)] = {"id": str(user_id), "name": random_string(40), "address": random_string(40), "phone": random_string(9, string.digits)}
      user_id += 1
      return None, 200

class User(Resource):
    def get(self, user_id):
      global users
      try: 
        user = users.get(str(user_id))
        time.sleep(random.uniform(0, 0.75))
        return make_response(json.dumps(user), 200)
      except:
        return None, 404

api.add_resource(User, '/<string:user_id>')
api.add_resource(Users, '/')

if __name__ == '__main__':
    app.run(debug=True)
